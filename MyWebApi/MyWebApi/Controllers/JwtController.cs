﻿using Microsoft.AspNetCore.Mvc;
using MyWebApi.Attributes;
using MyWebApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyWebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [ApiKey]
    public class JwtController : ControllerBase
    {
        [HttpGet]
        public IActionResult Jwt() => new ObjectResult(JwtToken.GenerateJwtToken());
    }
}
