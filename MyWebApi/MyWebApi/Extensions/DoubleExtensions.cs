﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyWebApi.Extensions
{
    public static class DoubleExtensions
    {
        public static int ToInt(this double input) => (int)input;
    }
}
