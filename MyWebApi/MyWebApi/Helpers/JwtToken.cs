﻿using Microsoft.IdentityModel.Tokens;
using MyWebApi.Extensions;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace MyWebApi.Helpers
{
    public static class JwtToken
    {
        private const string SECRET_KEY = "lw357EpRoF67zoLflkCZ";

        public static readonly SymmetricSecurityKey SIGNING_KEY = new(Encoding.UTF8.GetBytes(SECRET_KEY));

        public static string GenerateJwtToken()
        {
            var credentials = new SigningCredentials(SIGNING_KEY, SecurityAlgorithms.HmacSha256);
            var header = new JwtHeader(credentials);

            var expiry = DateTime.UtcNow.AddMinutes(1);
            var ts = (expiry - new DateTime(1970, 1, 1)).TotalSeconds.ToInt();

            var payload = new JwtPayload
            {
                { "sub", "testSubject"},
                { "Name", "Julien"},
                { "email", "julien.creach@protonmail.com"},
                { "exp", ts},
                { "iss", "https://localhost:44354"},
                { "aud", "https://localhost:44354"},
            };

            var secToken = new JwtSecurityToken(header, payload);

            var handler = new JwtSecurityTokenHandler();
            var tokenString = handler.WriteToken(secToken);

            if(tokenString.IsNullOrEmpty())
            {
                //TODO > Log : Error tokenString empty
                return string.Empty;
            }

            return tokenString;
        }
    }
}
